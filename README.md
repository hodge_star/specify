**specify** is a simple, single-file, header-only C++14 command-line argument
parser.

[![Join the chat at https://gitter.im/hodge_star/community](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/hodge_star/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

### Dependencies
There are no dependencies for including the single-header file in your project.
But the build system for the examples uses [meson](http://mesonbuild.com) and
the unit tests use [Catch2](https://github.com/catchorg/Catch2).

### Example usage

Consider the simple driver:
```c++
#include <iostream>
#include "specify.hpp"

int main(int argc, char* argv[]) {
  specify::ArgumentParser parser(argc, argv);
  const bool use_exclamation = parser.RequiredArgument<bool>(
      "use_exclamation",
       "Whether or not an exclamation mark should be used.");
  const int num_iterations = parser.OptionalArgument<int>(
      "num_iterations",
      "The number of times to print the statement.",
      3);
  if (!parser.OK()) {
    return 0;
  }

  // Print the statement the requested number of times.
  for (int iter = 0; iter < num_iterations; ++iter) {
    std::cout << "Ciao";
    if (use_exclamation) { std::cout << '!'; }
    std::cout << '\n';
  }

  return 0;
} 
```
Running this driver with the command-line arguments
```
--num_iterations=5 --use_exclamation=false
```
would produce
```
Ciao
Ciao
Ciao
Ciao
Ciao
```
while running with
```
--use_exclamation=true
```
would yield
```
Ciao!
Ciao!
Ciao!
```

Not specifying `use_exclamation` would lead to
`specify::ArgumentParser::OK()` returning false.

Please see `example/mandelbrot.cc` for a more detailed example. Its default
output should be:
```
ASCII iteration count buckets:
 : 0
.: 1-2.71828
,: 2.71828-7.38906
c: 7.38906-20.0855
j: 20.0855-54.5982
a: 54.5982-148.413
w: 148.413-403.429
r: 403.429-1096.63
p: 1096.63-2980.96
o: 2980.96-8103.08
g: 8103.08-22026.5
J: 22026.5-59874.1
O: 59874.1-162755
P: 162755-442413
Q: 442413-1.2026e+06
G: 1.2026e+06-3.26902e+06
E: 3.26902e+06-8.88611e+06
M: 8.88611e+06-2.4155e+07
             .....................................,,,,,,,,,,,.................|
            ..................................,,,,,,,,c,,,,,,,,,..............|
           ................................,,,,,,,,,,,,c,,,,,,,,,,............|
          ...............................,,,,,,,,,,,,,,cc,,,,,,,,,,,..........|
         ..............................,,,,,,,,,,,,,,,,cccjc,,,,,,,,,.........|
         .............................,,,,,,,,,,,,,,,,,ccjc,,,,,,,,,,,,.......|
        ............................,,,,,,,,,,,,,,,,,cccjjjc,,,,,,,,,,,,......|
       ...........................,,,,,,,,,,,,,,,,,ccjcjOajcjc,,,,,,,,,,,.....|
       ..........................,,,,,,,,,,,,,,,,,,,ccOOOOOOjc,,,,,,,,,,,,....|
      .........................,,,,,,,,,,,,,,,,,,,,ccjOOOOOOac,,,,,,,,,,,,....|
      ........................,,,,,,,,,,,,,,,c,,,cccccOOOOOjcccc,,,,,,c,,,,...|
     .......................,,,,,,,,,,,,,,ccjccccacjcjjjOOjjccccjc,,,,cc,,,,..|
     .....................,,,,,,,,,,,,,,,,ccjjjcjpwOOOOOOOOOOOOaOcccjcccc,,,..|
    .....................,,,,,,,,,,,,,,,,,cjaOOwOOOOOOOOOOOOOOOOOOcjOOjcc,,,,.|
    ...................,,,,,,,,,,,,,,,,,,ccccjOOOOOOOOOOOOOOOOOOOOOOOOcc,,,,,,|
    ................,,,,,,,,,,,,,,,,,,,ccacjOOOOOOOOOOOOOOOOOOOOOOOOacc,,,,,,,|
   .............,,,,,,,,,,,,,,,,,,,,,,,cjjOOOOOOOOOOOOOOOOOOOOOOOOOOOppc,,,,,,|
   .........,,,,,,,,,,,c,,,,,,c,,,,,,,,ccjOOOOOOOOOOOOOOOOOOOOOOOOOOOOwccj,,,,|
   ......,,,,,,,,,,,,,,ccccccccccc,,,ccjOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOj,,,,|
   ....,,,,,,,,,,,,,,,,cccaccccjcccccccapOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOjc,,,,,|
  ...,,,,,,,,,,,,,,,,,,cccOOjOoOOOcccccwOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOcc,,,,|
  ..,,,,,,,,,,,,,,,,,,,cccjOOOOOOOOaaccOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOjac,,,,|
  .,,,,,,,,,,,,,,,,,,cccjwOOOOOOOOOOOccOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOjc,,,,,|
  .,,,,,,,,,,,,,,,,cccccOOOOOOOOOOOOOOjOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOc,,,,,,|
  ,,,,,,,,,,,,,,,cccjjjjOOOOOOOOOOOOOOjOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOc,,,,,,|
  ,,,,,,ccc,cc,ccccjaOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOcc,,,,,,,|
  ,,,,,,ccc,cc,ccccjaOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOcc,,,,,,,|
  ,,,,,,,,,,,,,,,cccjjjjOOOOOOOOOOOOOOjOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOc,,,,,,|
  .,,,,,,,,,,,,,,,,cccccOOOOOOOOOOOOOOjOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOc,,,,,,|
  .,,,,,,,,,,,,,,,,,,cccjwOOOOOOOOOOOccOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOjc,,,,,|
  ..,,,,,,,,,,,,,,,,,,,cccjOOOOOOOOaaccOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOjac,,,,|
  ...,,,,,,,,,,,,,,,,,,cccOOjOoOOOcccccwOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOcc,,,,|
   ....,,,,,,,,,,,,,,,,cccaccccjcccccccapOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOjc,,,,,|
   ......,,,,,,,,,,,,,,ccccccccccc,,,ccjOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOj,,,,|
   .........,,,,,,,,,,,c,,,,,,c,,,,,,,,ccjOOOOOOOOOOOOOOOOOOOOOOOOOOOOwccj,,,,|
   .............,,,,,,,,,,,,,,,,,,,,,,,cjjOOOOOOOOOOOOOOOOOOOOOOOOOOOppc,,,,,,|
    ................,,,,,,,,,,,,,,,,,,,ccacjOOOOOOOOOOOOOOOOOOOOOOOOacc,,,,,,,|
    ...................,,,,,,,,,,,,,,,,,,ccccjOOOOOOOOOOOOOOOOOOOOOOOOcc,,,,,,|
    .....................,,,,,,,,,,,,,,,,,cjaOOwOOOOOOOOOOOOOOOOOOcjOOjcc,,,,.|
     .....................,,,,,,,,,,,,,,,,ccjjjcjpwOOOOOOOOOOOOaOcccjcccc,,,..|
     .......................,,,,,,,,,,,,,,ccjccccacjcjjjOOjjccccjc,,,,cc,,,,..|
      ........................,,,,,,,,,,,,,,,c,,,cccccOOOOOjcccc,,,,,,c,,,,...|
      .........................,,,,,,,,,,,,,,,,,,,,ccjOOOOOOac,,,,,,,,,,,,....|
       ..........................,,,,,,,,,,,,,,,,,,,ccOOOOOOjc,,,,,,,,,,,,....|
       ...........................,,,,,,,,,,,,,,,,,ccjcjOajcjc,,,,,,,,,,,.....|
        ............................,,,,,,,,,,,,,,,,,cccjjjc,,,,,,,,,,,,......|
         .............................,,,,,,,,,,,,,,,,,ccjc,,,,,,,,,,,,.......|
         ..............................,,,,,,,,,,,,,,,,cccjc,,,,,,,,,.........|
          ...............................,,,,,,,,,,,,,,cc,,,,,,,,,,,..........|
           ................................,,,,,,,,,,,,c,,,,,,,,,,............|
```

One can build and run the example via:
```
mkdir build/
meson build
cd build
ninja
./mandelbrot
```

### Running the unit tests
The [Catch2](https://github.com/catchorg/Catch2) test dependency
is automatically installed by [meson](http://mesonbuild.com).

One just has to run:
```
mkdir build-debug/
meson build-debug
cd build-debug
ninja
ninja test
```

### License
`specify` is distributed under the
[Mozilla Public License, v. 2.0](https://www.mozilla.org/media/MPL/2.0/index.815ca599c9df.txt).
