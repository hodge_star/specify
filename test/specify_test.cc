/*
 * Copyright (c) 2018 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#define CATCH_CONFIG_MAIN
#include "specify.hpp"
#include "catch2/catch.hpp"

// TODO(Jack Poulson): Determine if the following macro should be generalized
// and/or exported.
#ifdef __GNUG__
#define SPECIFY_UNUSED __attribute__((unused))
#elif defined(__clang__)
#define SPECIFY_UNUSED __attribute__((unused))
#endif  // ifdef __GNUG__

TEST_CASE("Simple example", "[simple]") {
  int argc = 3;
  const char* argv[] = {"./test", "--max_iter=100000", "--x_center=0.1"};

  specify::ArgumentParser parser(argc, argv);
  const int max_iter = parser.RequiredInput<int>(
      "max_iter", "The maximum number of iterations.");
  const int height = parser.OptionalInput<int>(
      "height", "The number of vertical pixels in the rendering.", 50);
  const int width = parser.OptionalInput<int>(
      "width", "The number of horizontal pixels in the rendering.", 78);
  const double x_center = parser.OptionalInput<double>(
      "x_center", "The horizontal center of the rendering.", -0.7);
  const double y_center = parser.OptionalInput<double>(
      "y_center", "The vertical center of the rendering.", 0.);
  const double x_span = parser.OptionalInput<double>(
      "x_span", "The horizontal span of the rendering.", 2.7);

  REQUIRE(parser.OK());
  REQUIRE(max_iter == 100000);
  REQUIRE(height == 50);
  REQUIRE(width == 78);
  REQUIRE(x_center == 0.1);
  REQUIRE(y_center == 0.);
  REQUIRE(x_span == 2.7);
}

TEST_CASE("Required failure", "[required failure]") {
  int argc = 2;
  const char* argv[] = {"./test", "--x_center=0.1"};

  specify::ArgumentParser parser(argc, argv);
  const int max_iter SPECIFY_UNUSED = parser.RequiredInput<int>(
      "max_iter", "The maximum number of iterations.");
  const int height SPECIFY_UNUSED = parser.OptionalInput<int>(
      "height", "The number of vertical pixels in the rendering.", 50);
  REQUIRE(!parser.OK());
}

TEST_CASE("Quote removal", "[quotes]") {
  int argc = 4;
  const char* argv[] = {"./test", "--a='alpha'", "--b=\"beta\"", "--c=gamma"};

  specify::ArgumentParser parser(argc, argv);
  const std::string a =
      parser.RequiredInput<std::string>("a", "The first Greek letter.");
  const std::string b =
      parser.RequiredInput<std::string>("b", "The second Greek letter.");
  const std::string c =
      parser.RequiredInput<std::string>("c", "The third Greek letter.");

  REQUIRE(parser.OK());
  REQUIRE(a == "alpha");
  REQUIRE(b == "beta");
  REQUIRE(c == "gamma");
}

TEST_CASE("Single space char (single)", "[single space char (single)]") {
  int argc = 2;
  const char* argv[] = {"./test", "--missing_char=' '"};

  specify::ArgumentParser parser(argc, argv);
  const char missing_char = parser.RequiredInput<char>(
      "missing_char", "The character to print for missing values.");
  REQUIRE(parser.OK());
  REQUIRE(missing_char == ' ');
}

TEST_CASE("Single space char (double)", "[single space char (double)]") {
  int argc = 2;
  const char* argv[] = {"./test", "--missing_char=\" \""};

  specify::ArgumentParser parser(argc, argv);
  const char missing_char = parser.RequiredInput<char>(
      "missing_char", "The character to print for missing values.");
  REQUIRE(parser.OK());
  REQUIRE(missing_char == ' ');
}
